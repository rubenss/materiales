# Algoritmos I

## Ordenación

Actividades:

* Presentación: el problema de la ordenación de listas.

**Ejercicio:** "Ordenación mediante selección"

Realiza un programa que ordene de menor a mayor una lista de números que se le proporcionen como argumentos en la línea de comandos. Por ejemplo:

```commandline
python3 sort.py 4 6 9 3 5 7
3 4 5 6 7 9
```

Para realizar la ordenación, utilizará el método de ordenación por selección:

* Para cada lugar en la lista, empezando por la izquierda:
  * Llamamos "lugar pivote" a este lugar
  * Busca, entre los números que estén más a la derecha del lugar pivote, el lugar en que esté el número más pequeño. Llamamos a este lugar encontrado "lugar del menor"
  * Intercambia el número que estaba en el lugar pivote con el número que estaba en el lugar del menor.

Solución: [sort.py](sort.py)

**Ejercicio:** "Ordenación mediante selección en cadena de caracteres"

Realiza un programa que ordene alfabéticamente los caracteres que estén en una cadena de caracteres (string) que se le proporcione como argumento en la línea de comandos. Por ejemplo:

```commandline
python3 sort_string.py HolaQueTal
aaeHlloQTu
```

Utiliza el algoritmo de ordenación por selección.

Ten en cuenta que las cadenas de caracteres (strings) son inmutables (no pueden cambiar). Por eso, convendrá que antes de ordenar los caracteres, obtengas una lista de caracteres a partir del string. Por ejemplo, si la cadena se llama `word`, puedes usar:

```python
word_chars = list(word)
```

Para realizar la operación inversa (obtener una cadena de caracteres a partir de una lista de caracteres) puedes usar:

```python
word = ''.join(word_chars)
```

Solución: [sortchars.py](sortchars.py)

**Ejercicio a entregar:** "Ordenacion de palabras"

[Enunciado](../ejercicios/README.md#ordenpalabras), incluyendo repositorio plantilla y fecha de entrega.

**Ejercicio:** "Ordenación mediante inserción"

Realiza un programa que ordene de menor a mayor una lista de números que se le proporcionen como argumentos en la línea de comandos, igual hicimos que en el ejercicio de ordenación mediante selección. Pero ahora lo vamos a hacer utilizando el algoritmo de ordenación mediante inserción:

* Para cada posición de la lista, `pos`:
  * Mientras la posición que esté a su izquierda tenga un elemento menor, intercambialos.

Solución: [sort_ins.py](sort_ins.py)

**Ejercicio:** "Ordenación mediante inserción en cadena de caracteres"

Realiza un programa que se comporte igual que el del ejercicio "Ordenación mediante selección de cadenas de caracteres", pero utilizando ordenación por inserción.

**Ejercicio a entregar:** "Ordenación de palabras mediante inserción"

[Enunciado](../ejercicios/README.md#ordenpalabras_ins), incluyendo repositorio plantilla y fecha de entrega.

**Ejercicio a entregar:** "Árboles por aproximación"

[Enunciado](../ejercicios/README.md#aproximacion), incluyendo repositorio plantilla y fecha de entrega.

** Ejercicio:** "Agenda"

Utilizando diccionarios, escribe un programa que implemente una agenda de contactos. 
El programa tendrá el siguiente menú para elegir cualquiera de sus opciones
ingresando un número según corresponda:

```
MI AGENDA
1) AGREGAR
2) MOSTRAR
3) MODIFICAR
4) ELIMINAR
5) BUSCAR
6) SALIR
SELECCIONA UNA OPCCIÓN __
```

El menú estará en el programa principal el mismo que llamará a las funciones como se describe a continuación. Basado en el menú cada opción se convertiría en una función, de esta manera

* Funcion Agregar: Insertará datos en la agenda, como Nombre, telefono y e-mail, si existiera 
el contacto nos dira que ya existe. y regresará al menu.
* Función Mostrar: Lista todos los contactos, en este caso el diccionario, luego retorna al menu.
* Función Modificar: Pedirá el nombre del contacto, si el usuario está en la agenda mostraraá 
el telefono y el email, adicional decir al usuario si quiere modificar (telefono y el email)
luego retornar al menu.
* Función Eliminar: Pedirá el nombre y si existe en la agenda, luego de un mensaje de confirmaciíon
se eliminará. luego retornar al menu.
* Función Buscar: Nos pedirá un nombre, y si existe nos mostrará los datos, de no existir
mostrará un mensaje diciendo que no existe.
* Finalmente al elegir Salir saldremos del programa tras un mensaje de Hasta pronto.

Datos adicionales. 

* Para controlar el menú , debe estar dentro de un ciclo `while`, para el mismo podemo usar un valor Booleano.
* Cada opción permitirá llamar a la función correspondiente enviando el diccionario.


**Ejercicio a entregar:** "Cantidades"

[Enunciado](../ejercicios/README.md#cantidades), incluyendo repositorio plantilla y fecha de entrega.

**Referencias:**

* [Sorting Algorithms (Visualization)](https://www.cs.usfca.edu/~galles/visualization/ComparisonSort.html)

