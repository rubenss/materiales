### Comandos de shell

* Muestra todo el contenido del directorio actual (Añade junto al comando una Captura de pantalla). 
* Crea desde terminal un nuevo directorio llamado "Entregable1". 
* Muévete a ese directorio y en él crea un archivo nuevo llamado "HolaMundo.txt". 
* Edita el archivo añadiendo una frase y guárdalo. 
* Lista en terminal el contenido del directorio Entregable1" (Añade junto al comando una Captura de pantalla). 
* Muestra el contenido del archivo "HolaMundo.txt" en el temrinal (Añade junto al comando una Captura de pantalla).
* Vuelve al directorio del apartado 1 y lista de nuevo todos los archivos disponibles. ¿Qúe ha cambiado?
