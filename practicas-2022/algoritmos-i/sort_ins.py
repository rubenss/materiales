#!/usr/bin/env pythoh3

"""Programa para ordenar enteros que se reciben como argumentos

Ordenación por inserción
"""

import sys

def find_lower(numbers: list, pivot: int) -> int:
    lower: int = pivot
    for pos in range(pivot, len(numbers)):
        if numbers[pos] < numbers [lower]:
            lower = pos
    return lower

def print_lista(numbers):
    for number in numbers:
        print(number, end=" ")
    print()

def main():
    numbers: list = sys.argv[1:]
    for pivot_pos in range(1, len(numbers)):
        current_pos = pivot_pos
        while (current_pos > 0) and (numbers[current_pos] < numbers[current_pos-1]):
            # Intercambia los números
            numbers[current_pos], numbers[current_pos-1] = \
                numbers[current_pos-1], numbers[current_pos]
            current_pos = current_pos - 1
            # Descomenta para ver una nueva lita cada vez que se mueven los números
            # print_lista(numbers)
        # Descomenta para ver una nueva lista para cada nuevo pivote
        # print_lista(numbers)
    print_lista(numbers)



if __name__ == '__main__':
    main()
