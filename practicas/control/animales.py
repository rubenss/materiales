#!/usr/bin/python3

# Escribe un programa que clasifique animales de acuerdo
# a dos características: grandes y pequeños por un lado,
# y rápidos y lentos por otro.
# El programa pedirá el nombre del animal, y escribirá si es
# grande o pequeño, y rápido o lento. Si no es uno de los
# animales que el programa considera, indicará "animal desconocido".
# Cuando esto haya terminado, se volverá a pedir otro nombre
# de animal, y así sucesivamente.
# Si en lugar de animal se pulsa simplemente la tecla "RETURN",
# el programa termina.

