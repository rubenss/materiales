### Cálculo de números primos

<!-- * **Fecha de entrega:** 19 de octubre de 2022, 23:59
* **Repositorio plantilla:** https://gitlab.etsit.urjc.es/cursoprogram/plantillas/primos -->

Construir un programa que solicite un número entero no negativo al usuario, escribiendo el mensaje "Dame un número entero no negativo: ", y que como respuesta escriba en una línea el mensaje "Números primos iguales o menores: ", y a continuación, en la misma línea, la lista de los números primos menores o iguales que él, separados por espacios. Para calcular los números primos, utiliza la definición: "un número entero es primo si sólo es divisible por 1 y por él mismo".

Un ejemplo de ejecución podría ser:

```commandline
Dame un número entero no negativo: 11
Números primos iguales o menores: 1 2 3 5 7 11
```

Otro podría ser:

```commandline
Dame un número entero no negativo: 12
Números primos iguales o menores: 1 2 3 5 7 11
```

Llama al programa `primos.py`

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.
