#!/usr/bin/env pythoh3

'''
Program to compute days, hours, minutes, seconds from a number of seconds
'''

SECONDS_IN_MIN = 60
SECONDS_IN_HOUR = 60 * SECONDS_IN_MIN
SECONDS_IN_DAY = 24 * SECONDS_IN_HOUR

# Read total number of seconds
tseconds = int(input("Número de segundos: "))

# Compute days, hours, minutes and seconds
days = tseconds // SECONDS_IN_DAY
seconds = tseconds - days * SECONDS_IN_DAY
hours = seconds // SECONDS_IN_HOUR
seconds = seconds - hours * SECONDS_IN_HOUR
minutes = seconds // SECONDS_IN_MIN
seconds = seconds - minutes * SECONDS_IN_MIN

# Print result
print(f"{tseconds} segundos son {days} día(s), {hours} hora(s), {minutes} minuto(s) y {seconds} segundo(s)")

