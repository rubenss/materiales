#!/usr/bin/env pythoh3

'''
Program to compute odd numbers between two given.
 This version is clearly inefficient, but it works.
 This version uses a function to determine if a number is prime.
'''

def is_prime(number):
    """Determine if number is prime (return True) or  not (return False)"""

    # Every number is prime until we can prove otherwise
    prime = True
    # Let's check if we find an exact divisor
    for divisor in range(2, number):
        if (number % divisor) == 0:
            # We found a divisor, number is not prime
            prime = False
            break;
    return prime

limit: int = int(input("Dame un número entero no negativo: "))

print("Números primos iguales o menores:", end='')

for number in range(1, limit+1):
    if is_prime (number):
        print('', number, end='')
