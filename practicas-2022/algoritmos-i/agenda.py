#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Escribir un programa que implemente una agenda de contactos. El
programa tendrá el siguiente menú:
Añadir - Modificar: El programa pedirá el nombre de la persona. Si el
nombre está en la agenda, debe mostrar el teléfono y pedir al usuario si
quiere elegir modificar su número.
Borrar: Nos pide un nombre y si existe en la agenda,
tras una confirmación lo borrará de la misma.
Listar: Nos mostrará todos los contactos de la agenda.
Salir: Finalizará el programa.
'''

import os
import sys

SALIR = 0
AGREGAR = 1
MOSTRAR = 2
BUSCAR = 3
MODIFICAR =4
ELIMINAR = 5

def mostrar_menu():
    #os.system('')
    print(f'''       MI AGENDA 
    {AGREGAR}) AGREGAR CONTACTO
    {MOSTRAR}) MOSTRAR CONTACTOS
    {BUSCAR}) BUSCAR CONTACTOS
    {MODIFICAR}) MODIFICAR CONTACTOS
    {ELIMINAR}) ELIMINAR CONTACTOS
    {SALIR}) SALIR ''')

def agregar_contactos(agenda):
    #os.sytem('')
    print('               AGREGAR CONTACTO')
    nombre = input("NOMBRE: ")
    if agenda.get(nombre):
        print("YA EXISTE EL CONTACTO")
    else:
        telefono= input("TELEFONOS")
        email = input ("EMAIL:")
        agenda.setdefault(nombre,(telefono,email))
        print("CONTACTO AGREGADO CON ÉXITO ")

def mostrar_contactos(agenda):
    #os.sytem('')
    print('               MOSTRAR CONTACTOS')
    if len(agenda) > 0:
        for contacto, datos in agenda.items():
            print(f'NOMBRE: {contacto}')
            print(f'TELEFONO: {datos[0]}')
            print(f'EMAIL: {datos[1]}')
            print('~'*50)
    else:
        print("NO HAY CONTACTOS REGISTRADOS")

def buscar_contactos(agenda):
    #os.sytem('')
    print('               BUSCAR CONTACTOS')
    if len(agenda) > 0:
        nombre = input("NOMBRE: ")
        encontrados=0
        for contacto, datos in agenda.items():
            if nombre in contacto:
                print(f'NOMBRE: {contacto}')
                print(f'TELEFONO: {datos[0]}')
                print(f'EMAIL: {datos[1]}')
                print('~'*50)
                encontrados +=1
        if encontrados==0 :
            print("NO SE ENCONTRO EL CONTACTO")
        else:
            print(f'SE ENCONTRARON{encontrados} CONTACTOS.')
    else:
        print("NO HAY CONTACTOS REGISTRADOS")

def modificar_contactos(agenda):
    #os.sytem('')
    print('               MODIFICAR CONTACTOS')
    if len(agenda) > 0:
        nombre = input("NOMBRE: ")
        if agenda.get(nombre):
            datos=agenda.get(nombre)
            print("INFORMACION ACTUAL:")
            print(f'NOMBRE: {nombre}')
            print(f'TELEFONO: {datos[0]}')
            print(f'EMAIL: {datos[1]}')
            print('~'*50)
            print("INGRESA LOS NUEVOS DATOS:")
            telefono = input("TELEFONO:")
            email = input("EMAIL:")
            agenda[nombre] = (telefono, email)
            print("DATOS ACTUALIZADOS CON EXITO")
        else:
            print("NO EXISTE EL CONTACTO")
    else:
        print("NO HAY CONTACTOS REGISTRADOS")

def eliminar_contactos(agenda):
    #os.sytem('')
    print('               ELIMINAR CONTACTO')
    if len(agenda) > 0:
        nombre = input("NOMBRE: ")
        if(agenda.get(nombre)):
            agenda.pop(nombre)
            print("CONTACTO ELIMINADO")
    else:
        print("NO HAY CONTACTOS REGISTRADOS")
def main():
    continuar = True
    mi_agenda={}
    while continuar == True:
        mostrar_menu()
        opc = int(input("SELECCIONA UNA OPCION: "))
        if opc == AGREGAR:
            agregar_contactos(mi_agenda)
        elif opc == MOSTRAR:
            mostrar_contactos(mi_agenda)
        elif opc == MODIFICAR:
            modificar_contactos(mi_agenda)
        elif opc == BUSCAR:
            buscar_contactos(mi_agenda)
        elif opc == ELIMINAR:
            eliminar_contactos(mi_agenda)
        elif opc == SALIR:
            continuar = False
        else:
            print("OPCION NO VALIDA")
            input("PRECIONA ENTER PARA CONTINUAR...")
    print("HASTA PRONTO")


if __name__ == '__main__':
    main()















    
    

