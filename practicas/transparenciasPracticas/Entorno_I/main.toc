\babel@toc {nil}{}\relax 
\beamer@sectionintoc {1}{Entorno I: Laboratorios de Linux y PyCharm como IDE}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Introducción}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Laboratorios Linux URJC}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Linux como Sistema Operativo}{8}{0}{1}
\beamer@subsectionintoc {1}{4}{Introducción a Shell}{13}{0}{1}
\beamer@subsectionintoc {1}{5}{Intérprete de Python}{19}{0}{1}
\beamer@subsectionintoc {1}{6}{PyCharm como IDE}{20}{0}{1}
